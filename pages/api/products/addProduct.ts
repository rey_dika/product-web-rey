import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "POST") {
    // Handle the POST request
    const {nama, deskripsi, harga, stok, foto, suplier_id} = req.body;

    if (!nama || !deskripsi || !harga || !stok || !foto || !suplier_id) {
        return res.status(400).json({ error: 'nama, deskripsi,, harga, stok, foto, and suplier_id are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('INSERT INTO produk (nama, deskripsi, harga, stok, foto, suplier_id) VALUES (?, ?, ?, ?, ?, ?)', nama, deskripsi, harga, stok, foto, suplier_id);
    return res.status(201).json({ id: result.lastID, nama, deskripsi, harga, stok, foto, suplier_id });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
