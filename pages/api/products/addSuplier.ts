import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "POST") {
    // Handle the POST request
    const {nama_suplier, alamat, email} = req.body;

    if (!nama_suplier || !alamat || !email) {
        return res.status(400).json({ error: 'nama suplier, alamat and email are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('INSERT INTO suplier (nama_suplier, alamat, email) VALUES (?, ?, ?)', nama_suplier, alamat, email);
    return res.status(201).json({ id: result.lastID, nama_suplier, alamat, email });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
