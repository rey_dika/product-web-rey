import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "DELETE") {
    // Handle the POST request
    const {id} = req.body;

    if (!id) {
        return res.status(400).json({ error: 'id are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('DELETE FROM produk WHERE id = ?', id);
    return res.status(200).json({ message: `produk with id ${id} has been removed` });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
