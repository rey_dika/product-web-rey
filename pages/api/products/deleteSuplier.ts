import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "DELETE") {
    // Handle the POST request
    const {id_suplier} = req.body;

    if (!id_suplier) {
        return res.status(400).json({ error: 'id_suplier are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('DELETE FROM suplier WHERE id_suplier = ?', id_suplier);
    return res.status(200).json({ message: `suplier with id ${id_suplier} has been removed` });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
