import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const db = new sqlite3.Database("./test.db");
  db.all("SELECT * FROM produk", (err, rows) => {
    if (err) {
      res.status(500).json({ error: "Internal Server Error" });
    } else {
      res.status(200).json(rows);
    }
  })
}
