import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "PUT") {
    // Handle the POST request
    const {nama, deskripsi, harga, stok, foto, suplier_id, id} = req.body;

    if (!nama || !deskripsi || !harga || !stok || !foto || !suplier_id) {
        return res.status(400).json({ error: 'nama, deskripsi,, harga, stok, foto, and suplier_id are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('UPDATE produk SET nama = ?, deskripsi = ?, harga = ?, stok = ?, foto = ?, suplier_id = ? WHERE id = ?', nama, deskripsi, harga, stok, foto, suplier_id, id);
    return res.status(201).json({ id: result.lastID, nama, deskripsi, harga, stok, foto, suplier_id });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
