import { NextApiRequest, NextApiResponse } from "next";
import sqlite3 from "sqlite3";

export default async function readProduct(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "PUT") {
    // Handle the POST request
    const {id_suplier, nama_suplier, alamat, email} = req.body;

    if (!nama_suplier || !alamat || !email) {
        return res.status(400).json({ error: 'nama_suplier, alamat, and email are required.' });
    }

    const db = new sqlite3.Database("./test.db");
    const result = await db.run('UPDATE suplier SET nama_suplier = ?, alamat = ?, email = ? WHERE id_suplier = ?', nama_suplier, alamat, email, id_suplier);
    return res.status(201).json({ id: result.lastID, nama_suplier, alamat, email });
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
