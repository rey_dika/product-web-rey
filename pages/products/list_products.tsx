import React, { useState, useEffect } from "react";

export default function ListProducts() {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchData();
  }, []);

  interface Ipost {
    id: number,
    nama: string,
    foto: string,
    harga: number,
    stok: number,
    suplier_id: number
  }

  const fetchData = async () => {
    try {
      const response = await fetch(
        "http://localhost:3000/api/products/getProduct"
      );
      const result: Ipost[] = await response.json();
      console.log(result, "lalala");
      setData(result);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  return (
    <div>
      <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" class="px-6 py-3">
                Nama Produk
              </th>
              <th scope="col" class="px-6 py-3">
                Deskripsi
              </th>
              <th scope="col" class="px-6 py-3">
                Harga
              </th>
              <th scope="col" class="px-6 py-3">
                Stok 
              </th>
              <th scope="col" class="px-6 py-3">
                Foto 
              </th>
              <th scope="col" class="px-6 py-3">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {console.log(data, "haolo")}
            
              <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-gray-800 border-b dark:border-gray-700">
              {data.map((datas) => {
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                >
                  halo
                </th>
                <td className="px-6 py-4">halo</td>
                <td className="px-6 py-4">halo</td>
                <td className="px-6 py-4">halo</td>
                <td className="px-6 py-4">halo</td>
                <td className="px-6 py-4">
                  <a
                    href="#"
                    className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                  >
                    Edit
                  </a>
                </td>
                })}
              </tr>;
          </tbody>
        </table>
      </div>

      {/* {data.map((data, index) => { */}
      {/* <div className="mx-auto mt-11 w-80 transform overflow-hidden rounded-lg bg-white dark:bg-slate-800 shadow-md duration-300 hover:scale-105 hover:shadow-lg">
          <img
            className="h-48 w-full object-cover object-center"
            src="https://images.unsplash.com/photo-1674296115670-8f0e92b1fddb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"
            alt="Product Image"
          />
          <div className="p-4">
            <h2 className="mb-2 text-lg font-medium dark:text-white text-gray-900">
              Product Name
            </h2>
            <p className="mb-2 text-base dark:text-gray-300 text-gray-700">
              Product description goes here.
            </p>
            <div className="flex items-center">
              <p className="mr-2 text-lg font-semibold text-gray-900 dark:text-white">
                $20.00
              </p>
              <p className="text-base  font-medium text-gray-500 line-through dark:text-gray-300">
                $25.00
              </p>
              <p className="ml-auto text-base font-medium text-green-500">
                20% off
              </p>
            </div>
          </div>
        </div>; */}
      {/* })} */}
    </div>
  );
}
